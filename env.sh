#!/bin/bash

export ULSI_TMP_DIR="${ULSI_PREFIX}/ulsi-tmp"
export ULSI_LOG_DIR="${ULSI_PREFIX}/ulsi-log"
export ULSI_CACHE_DIR="${ULSI_PREFIX}/ulsi-cache"
export ULSI_PKGINFO_DIR="${ULSI_CACHE_DIR}/pkg-info"
export ULSI_REPO_DIR="${ULSI_PREFIX}/ulsi"
export ULSI_BIN_DIR="${ULSI_REPO_DIR}/bin"

#export ULSI_DEBUG="false"

export PATH="${PATH}:${ULSI_BIN_DIR}"

export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ULSI_PREFIX}/usr/lib/x86_64-linux-gnu"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ULSI_PREFIX}/usr/lib/x86_64-linux-gnu/wine"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ULSI_PREFIX}/usr/lib/x86_64-linux-gnu/android"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ULSI_PREFIX}/usr/lib/i386-linux-gnu"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ULSI_PREFIX}/usr/lib/i386-linux-gnu/wine"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ULSI_PREFIX}/usr/lib/i386-linux-gnu/pulseaudio"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ULSI_PREFIX}/usr/lib/virtualbox"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ULSI_PREFIX}/usr/lib/wine"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ULSI_PREFIX}/usr/lib"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ULSI_PREFIX}/lib"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ULSI_PREFIX}/usr/lib32"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ULSI_PREFIX}/usr/lib64"
#export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ULSI_PREFIX}/lib/x86_64-linux-gnu" # THIS LINE CAUSES SEGFAULTS!!!
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ULSI_PREFIX}/lib/i386-linux-gnu"
export LIBPATH="${LIBPATH}:${LD_LIBRARY_PATH}"
export PATH="${PATH}:${ULSI_PREFIX}/bin"
export PATH="${PATH}:${ULSI_PREFIX}/sbin"
export PATH="${PATH}:${ULSI_PREFIX}/usr/bin"
export PATH="${PATH}:${ULSI_PREFIX}/usr/sbin"
export PATH="${PATH}:${ULSI_PREFIX}/usr/local/bin"
export PATH="${PATH}:${ULSI_PREFIX}/usr/local/sbin"
export PATH="${PATH}:${ULSI_PREFIX}/usr/games"
export PATH="${PATH}:${ULSI_PREFIX}/node_modules/.bin"
export PATH="${PATH}:${ULSI_PREFIX}/usr/share/code/bin"
export CPATH="${CPATH}:${ULSI_PREFIX}/usr/include"
export CPATH="${CPATH}:${ULSI_PREFIX}/usr/include/x86_64-linux-gnu"
export CPATH="${CPATH}:${ULSI_PREFIX}/usr/include/x86_64-linux-gnu/curl"
export CPATH="${CPATH}:${ULSI_PREFIX}/usr/include/SDL"
export CPATH="${CPATH}:${ULSI_PREFIX}/usr/include/GL"
export CPATH="${CPATH}:${ULSI_PREFIX}/usr/include/X11"
export CPATH="${CPATH}:${ULSI_PREFIX}/usr/include/X11/extensions"
export CPPPATH="${CPATH}:${CPPPATH}"

export PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:${ULSI_PREFIX}/usr/lib/x86_64-linux-gnu/pkgconfig"
export PKG_CONFIG_PATH="${PKG_CONFIG_PATH}:${ULSI_PREFIX}/usr/lib/i386-linux-gnu/pkgconfig"

# siehe https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
export XDG_DATA_DIRS="${XDG_DATA_DIRS}:${ULSI_PREFIX}/usr/local/share:${ULSI_PREFIX}/usr/share"

export INFOPATH="${INFOPATH}:${ULSI_PREFIX}/usr/local/share/info"
export INFOPATH="${INFOPATH}:${ULSI_PREFIX}/usr/share/info"
export MANPATH="${MANPATH}:${ULSI_PREFIX}/usr/local/share/man"
export MANPATH="${MANPATH}:${ULSI_PREFIX}/usr/share/man"

export DICPATH="${DICPATH}:${ULSI_PREFIX}/usr/share/hunspell"
export DICPATH="${DICPATH}:${ULSI_PREFIX}/usr/share/myspell"
export DICPATH="${DICPATH}:${ULSI_PREFIX}/usr/share/myspell/dicts"






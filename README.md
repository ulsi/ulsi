# ULSI - Ubuntu Local Software Installer

Tool for installing software on Ubuntu locally in your HOME directory without root permissions.

[[_TOC_]]

## System Requirements & Dependencies

- Ubuntu (Tested on: Ubuntu 20.04.3 LTS 64bit)
- `git`
- `coreutils`
- `bash`
- `sed`
- `grep`
- `dpkg`
- `apt-utils`
- `apt`

## How-To: Install ULSI

1. (Install dependencies: `apt-get install git coreutils bash sed grep dpkg apt-utils apt`)
1. Create an ulsi prefix directory: `mkdir ulsi-prefix`
1. Go into the ulsi prefix directory: `cd ulsi-prefix/`
1. Get the full ulsi prefix directory path: `pwd`
1. Clone the repo: `git clone https://gitlab.gwdg.de/ulsi/ulsi.git`
1. Go into the ulsi directory: `cd ulsi/`
1. Run `./setup.sh` and follow the instructions.
1. **It is recommended to reboot your system now (or logout and login again).**

Or just execute this one simple command:
```bash
mkdir ulsi-prefix && cd ulsi-prefix/ && pwd && git clone https://gitlab.gwdg.de/ulsi/ulsi.git && cd ulsi/ && ./setup.sh
```

![ULSI Installation](images/setup.png)

## How-To: Install Software

Just run `ulsi-install <package> [package ...]`.
That's it.

![Installing nodejs with ulsi-install](images/install-example-nodejs.png)

### How-To: Install 32-bit Software

Just run `ulsi-install <package>:i386 [package:i386 ...]`.
That's it.

### How-To: Install Software from a .deb File

Just run `ulsi-install-deb <deb file/url>` and afterwards install the dependencies using `ulsi-install <package> [packge ...]`.
That's it.

![Installing Rocketchat with ulsi-install-deb](images/install-deb-example-rocketchat.png)

## How-To: Update Software

Just run `ulsi-update`.
That's it.

Note: This will also update ULSI itself.

Note: This will NOT update software that was installed using `ulsi-install-deb`.

![Updating ULSI and Packages](images/update.png)


## How-To: Find Software

Just use `apt-cache search <regex>` or `apt search <regex>`.


## Structure of the ulsi prefix directory

- `ulsi-prefix/ulsi/`: Installation directory of ULSI.
- `ulsi-prefix/ulsi-cache/`: Cache directory for ULSI.
- `ulsi-prefix/ulsi-log/`: Log directory for ULSI.
- `ulsi-prefix/ulsi-tmp/`: Tmp directory for ULSI.
- `ulsi-prefix/`: Installation directory for all the software.


## Disclaimer

Quite a lot of software is not going to work when installed with this tool, but some stuff will work really well.

For example `nodejs` works really well, but most pyhton packages don't work at all.

Use it at your own risk.



#!/bin/bash

# Exit on error
set -e

function print_error_and_exit() {
	echo -e "\n\e[35m=> \e[31m[ERROR] ${1}\e[0m\n" >&2
	exit 1
}

function print_task() {
	echo -e "\e[35m=> \e[33m[TASK] \e[0m${1}\e[0m" >&2
}

function print_question() {
	echo -e "\e[35m=> \e[34m[QUESTION] \e[0m${1}\e[0m" >&2
}
function print_info() {
	echo -e "\e[35m=> \e[36m[INFO] \e[0m${1}\e[0m" >&2
}
function print_warn() {
	echo -e "\n\e[35m=> \e[33m[WARN] ${1}\e[0m\n" >&2
}

function print_debug() {
	if [ "${ULSI_DEBUG}" = "true" ]; then
		echo -e "\e[35m=> \e[34m[DEBUG] \e[0m${1}\e[0m" >&2
	fi
}

function ask_yon() {
	local yon=""
	while true ; do
		printf '\r'
		read -p "[Y/N]? " -N 1 -r yon
		if [ "x${yon}" = "xy" ] || [ "x${yon}" = "xY" ]; then
			printf '\n'
			true; return
		elif [ "x${yon}" = "xn" ] || [ "x${yon}" = "xN" ]; then
			printf '\n'
			false; return
		fi
	done
}

print_info "Starting ULSI Setup $(date)"

if [ "${ULSI_UPDATE_MODE}" = "true" ]; then
	print_info "ULSI_UPDATE_MODE is set to true. => Running in Update-Mode."
fi

pushd "$(dirname "$0")" >/dev/null

TMP_DIR="$(mktemp -d)"

pushd ".." >/dev/null
export ULSI_PREFIX="$(pwd)"
popd >/dev/null
if [ "${ULSI_UPDATE_MODE}" != "true" ]; then
	print_question "Is \"${ULSI_PREFIX}\" your desired ULSI-Prefix?"
	if ! ask_yon ; then
		print_error_and_exit "Please read the installation instructions carefully!"
	fi
fi

print_info "Using \"${ULSI_PREFIX}\" as ULSI-PREFIX."

export PROFILEFILE=""
if [ "x${ULSI_OVERRIDE_PROFILEFILE}" != "x" ]; then
	PROFILEFILE="${ULSI_OVERRIDE_PROFILEFILE}"
elif [ -f "${HOME}/.bash_profile" ]; then
	PROFILEFILE="${HOME}/.bash_profile"
elif [ -f "${HOME}/.bash_login" ]; then
	PROFILEFILE="${HOME}/.bash_login"
elif [ -f "${HOME}/.profile" ]; then
	PROFILEFILE="${HOME}/.profile"
else
	print_question "You don't seem to have a startup file. Do you want me to create one (~/.profile)?"
	if ! ask_yon ; then
		print_error_and_exit "You need a startup file (~/.bash_profile, ~/.bash_login, or ~/.profile)."
	fi
	PROFILEFILE="${HOME}/.profile"
	touch "${PROFILEFILE}"
fi
print_info "Using \"${PROFILEFILE}\" as startup file."

CAN_WRITE_ULSI_START_COMMAND="true"

if grep -q '^export ULSI_PREFIX=' "${PROFILEFILE}" ; then
	print_info "Your startup file already contains an ULSI-Load-Command."
	grep -H '^export ULSI_PREFIX=' "${PROFILEFILE}"
	print_task "Removing old ULSI-Load-Command"
	[ "x$(cat "${PROFILEFILE}" | grep -v '^export ULSI_PREFIX=' | wc -l)" = "x0" ] && echo "" >> "${PROFILEFILE}" # Fix bug where the following line crashes on empty file
	cat "${PROFILEFILE}" | grep -v '^export ULSI_PREFIX=' > "${TMP_DIR}/profile"
	cp "${TMP_DIR}/profile" "${PROFILEFILE}"
else
	print_question "May I update your startup file to include the ULSI-Load-Command?"
	if ! ask_yon ; then
		CAN_WRITE_ULSI_START_COMMAND="false"
	fi
fi

if [ "${CAN_WRITE_ULSI_START_COMMAND}" = "true" ]; then
	print_task "Adding ULSI-Load-Command to your startup file."
	echo "export ULSI_PREFIX=\"${ULSI_PREFIX}\"; [ -d \"\${ULSI_PREFIX}\" ] && . \"\${ULSI_PREFIX}/ulsi/env.sh\" ; # ULSI-Load-Command" >> "${PROFILEFILE}"
else
	print_warn "You need to execute 'export ULSI_PREFIX=\"${ULSI_PREFIX}\"; . \"\${ULSI_PREFIX}/ulsi/env.sh\"' manually to load ULSI!"
fi

print_task "Updating local environment"
. "${ULSI_PREFIX}/ulsi/env.sh"

print_task "Creating directories"
mkdir -p "${ULSI_PREFIX}"
mkdir -p "${ULSI_TMP_DIR}"
mkdir -p "${ULSI_LOG_DIR}"
mkdir -p "${ULSI_REPO_DIR}"
mkdir -p "${ULSI_CACHE_DIR}"
mkdir -p "${ULSI_PKGINFO_DIR}"
mkdir -p "${ULSI_BIN_DIR}"

print_info "Finished ULSI Setup! $(date)"
if [ "${ULSI_UPDATE_MODE}" != "true" ]; then
	print_info "You can now restart your computer or run 'source \"${PROFILEFILE}\"' to load ULSI."
fi
popd >/dev/null

rm -rf "${TMP_DIR}"
